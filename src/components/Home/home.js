import React, {Component} from 'react';
import {getBlogs} from '../../redux/actions/blogs';
import {getMostViewedDomains} from '../../redux/actions/domains';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

class Home extends Component {
    constructor (props){
        super(props);
        this.props.getBlogs();
        this.props.getMostViewedDomains();
    }

    static propTypes = {
        getBlogs: PropTypes.func.isRequired,
        getMostViewedDomains: PropTypes.func.isRequired,
        mostViewedDomains: PropTypes.array,
        blogs: PropTypes.array,
    }

    render(){
        console.log(this.props.mostViewedDomains);
        return (
            <div>
                <h1>hello world!</h1>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    blogs: state.blogs.all_blogs,
    mostViewedDomains: state.domains.most_viewed_domains
});

export default connect(mapStateToProps, {getBlogs, getMostViewedDomains})(Home);