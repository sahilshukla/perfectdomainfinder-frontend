import {GET_MOST_VIEWED_DOMAINS} from '../actions/types';


const initialState = {
    most_viewed_domains: {},
}


export default function(state=initialState, action){
    switch(action.type){
        case GET_MOST_VIEWED_DOMAINS:
            return {
                ...state,
                most_viewed_domains: action.payload,
            }
            default:
                return state;
    }
}