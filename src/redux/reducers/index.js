import { combineReducers } from "redux";
import blogs from './blogs';
import domains from './domains';

export default combineReducers({
    blogs,
    domains,
});
